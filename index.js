import ReactDOM from 'react-dom';
import Root from './app/containers/Root';
import Home from './app/containers/Home';
import Details from './app/containers/Details';
import React from 'react';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import AppReducer from './app/reducers/AppReducer';
import callApiMiddleware from './app/utils/Middleware';
import {Router, Route} from 'react-router';
import {syncHistory, routeReducer} from 'redux-simple-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import useBasename from 'history/lib/useBasename';

let baseName = '/';
const history = useBasename(createBrowserHistory)({
    basename: baseName
});

//const reducers = [AppReducer];
const combinedReducers = combineReducers({AppReducer, routing: routeReducer});
const reduxRouterMiddleware = syncHistory(history);
const middles = [callApiMiddleware, reduxRouterMiddleware];
const createStoreWithMiddleware = applyMiddleware(...middles)(createStore);
let myStore = createStoreWithMiddleware(combinedReducers);

reduxRouterMiddleware.listenForReplays(myStore);

ReactDOM.render(
 <Provider store={myStore}>
         <Router history={history}>
             <Route component={Root} name='root'>
                 <Route component={Home} path='/'/>
                 <Route component={Details} path='/details'/>
             </Route>
         </Router>
 </Provider>,
 document.getElementById('root')
);

import * as types from '../constants/ActionTypes';

export function getInstances (dispatch) {
    return {
        dispatch,
        type: types.DESCRIBE_INSTANCES,
        promise: 'ec2.describeInstances'
    };
}

export function updateInstances (retInstance) {
    return {
        type: types.UPDATE_INSTANCES,
        retInstance
    };
}

export function returnFiltered (instances, filter) {
    return {
        type: types.FILTER_INSTANCES,
        instances,
        filter
    };
}

export function getMetricsList (instance, dispatch) {
    return {
        instance,
        dispatch,
        type: types.GET_METRICS,
        promise: 'getMetricsList'
    };
}

export function getInstanceMetrics (metrics, instance, dispatch) {
    return {
        metrics,
        instance,
        dispatch,
        type: types.GET_METRIC_VALUES,
        promise: 'ec2.getMetrics'
    };
}

export function setInstance (instance) {
    return {
        type: types.SELECT_INSTANCE,
        instance
    };
}

export function updateMetrics (metrics) {
    return {
        type: types.UPDATE_METRICS,
        metrics
    };
}

export function checkAlarms (dispatch) {
    return {
        dispatch,
        type: types.CHECK_ALARMS,
        promise: 'cw.checkAlarms'
    };
}

export function showPopup (alarms) {
    return {
        type: types.SHOW_POPUP,
        alarms
    };
}

export function hidePopup () {
    return {
        type: types.HIDE_POPUP
    };
}
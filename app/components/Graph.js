import React, {Component, PropTypes} from 'react';
import {Col} from 'react-bootstrap';
import {Line} from 'react-chartjs';
//
//const styles = {
//    graph: {
//        width: '800px',
//        maxWidth: '100%',
//        height: '600px'
//    }
//};

export default class Chart extends Component {
    renderChart (data) {
        let points = [];
        let labels = [];
        data.Datapoints.sort((x, y) => {
            let xTime = new Date(x.Timestamp);
            let yTime = new Date(y.Timestamp);
            return xTime - yTime;
        });
        data.Datapoints.forEach((point) =>{
            points.push(point.Sum);
            let time = new Date(point.Timestamp);
            let hour = time.getHours();
            let minutes = time.getMinutes();
            labels.push(hour + '.' + minutes);
        });
        let options = {};
        points.sort((x, y) => {
            return x.timestamp - y.timestamp;
        });
        let graphData = {
            labels: labels,
            datasets: [{
                label: data.Label,
                fillColor: 'rgba(255, 153, 51,0.2)',
                strokeColor: 'rgba(255, 153, 51,1)',
                pointColor: 'rgba(255, 153, 51,1)',
                pointStrokeColor: 'rgb(255, 153, 51)',
                pointHighlightFill: '#ff0000',
                pointHighlightStroke: '#ff0000',
                data: points
            }]
        };
        return(
            <Col sm={12}>
                <h4>{data.Label}</h4>
                <Line data={graphData} options={options} width="1400" height="600"/>
            </Col>
        );
    }
    render () {
        const {data} = this.props;
        return (
            <div>
                {this.renderChart(data)}
            </div>
        );
    }
}

Chart.Proptypes = {
    data: PropTypes.object.isRequired
};
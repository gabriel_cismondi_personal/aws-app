import React, {Component, PropTypes} from 'react';
import {Row, PageHeader} from 'react-bootstrap';

export const styles = {
    header: {
        textAlign: 'center'
    }
};
export default class Header extends Component {
    render () {
        const {title, subTitle} = this.props;
        return (
            <Row style={styles.header}>
                <PageHeader>{title}<br/>
                    <small>{subTitle}</small>
                </PageHeader>
            </Row>
        );
    }
}
Header.propTypes = {
    subTitle: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
};
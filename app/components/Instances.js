import React, {Component, PropTypes} from 'react';
//import {Col, Glyphicon} from 'react-bootstrap';
import Instance from './Instance';

const styles = {
    instancesBox: {
        textAlign: 'center'
    }
};

export default class Instances extends Component {
    render () {
        const {instances, actions, dispatch} = this.props;
        return (
            <div style={styles.instancesBox}>
                {instances.map((item, i) => {
                    return (
                        <Instance instanceData={item} actions={actions} dispatch={dispatch} Key={i}/>
                    );
                })}
            </div>
        );
    }
}

Instances.PropTypes = {
    instances: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
};

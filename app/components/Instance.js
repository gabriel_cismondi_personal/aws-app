import React, {Component, PropTypes} from 'react';
import {Col, Glyphicon} from 'react-bootstrap';
import {routeActions} from 'redux-simple-router';

const styles = {
    padTop : {
        paddingTop: 30
    }
};

export default class Instance extends Component {
    handleClick () {
        this.props.actions.setInstance(this.props.instanceData);
        this.props.dispatch(routeActions.push('/details'));
    }
    render () {
        const {instanceData} = this.props;
        return (
             <Col sm={6} style={styles.padTop}>
                <h3 onClick={() => {
                    this.handleClick();
                }}>
                    <Glyphicon bsSize='large' glyph='th'/><br/>
                    {instanceData.Tags.map((tag) => {
                        if (tag.Key == 'Name') return tag.Value;
                    })}<br/>
                </h3>
                <h4>
                    {instanceData.InstanceId}<br/>
                    <small>{instanceData.Region}</small>
                </h4>
            </Col>
        );
    }
}

Instance.PropTypes = {
    instanceData: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
};

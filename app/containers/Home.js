import React, {Component, PropTypes} from 'react';
import Header from '../components/Header';
import Instances from '../components/Instances';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../actions/Actions';
import {Button, Modal} from 'react-bootstrap';
import {routeActions} from 'redux-simple-router';

const styles = {
    padButton: {
        marginLeft: 10
    }
};

class Home extends Component {
    componentWillMount () {
        this.props.actions.checkAlarms(this.props.dispatch);
        this.props.actions.getInstances(this.props.dispatch);
    }
    render () {
        const {instances, filtered, actions, dispatch, showPopup, alarms} = this.props;
        return (
            <div>
                <Modal show={showPopup} onHide={() => actions.hidePopup()}>
                    <Modal.Header closeButton>
                        <Modal.Title>You have Alarms</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Check the following alarms...
                        {alarms.map((item) => {
                            return (
                                <div>
                                    {item.AlarmName}
                                </div>
                            );
                        })}
                    </Modal.Body>
                </Modal>
                <Header title=' AWS Smart View' subTitle='running instances'/>
                <p>Filter by Region
                    <Button style={styles.padButton} bsStyle='info' onClick={() => {
                        dispatch(actions.returnFiltered(instances, {region : 'eu-west-1', tag: ''}));
                    }}>EU-WEST-1</Button>
                    <Button style={styles.padButton} bsStyle='info' onClick={() => {
                        dispatch(actions.returnFiltered(instances, {region : 'eu-central-1', tag: ''}));
                    }}>EU-CENTRAL-1</Button>
                    <Button style={styles.padButton} bsStyle='info' onClick={() => {
                        dispatch(routeActions.push('/details'));
                    }}>METRICS</Button>
                </p>
                <Instances instances={filtered} actions={actions} dispatch={dispatch}/>
            </div>
        );
    }
}

Home.PropTypes = {
    actions: PropTypes.object.isRequired,
    instances: PropTypes.array.isRequired,
    showPopup: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
    filtered: PropTypes.array.isRequired,
    alarms: PropTypes.array.isRequired
};

function mapStateToProps ({AppReducer}) {
    return {
        instances: AppReducer.instances,
        filtered: AppReducer.filtered,
        showPopup: AppReducer.showPopup,
        alarms: AppReducer.alarms
    };
}

function mapDispatchToProps (dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch),
        dispatch
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

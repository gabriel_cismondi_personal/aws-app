import React, {Component, PropTypes} from 'react';
import Header from '../components/Header';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../actions/Actions';
//import Instance from '../components/Instance';
import Chart from '../components/Graph';
import {Col, Glyphicon} from 'react-bootstrap';
import {routeActions} from 'redux-simple-router';

class Details extends Component {
    componentWillMount () {
        this.props.actions.getMetricsList(this.props.selectedInstance, this.props.dispatch);
    }
    render () {
        const {metrics} = this.props;
        return (
            <div>
                <Header title='ENEL METRICS' subTitle='metrics'/>
                <Col sm={12} style={{textAlign: 'center'}}>
                    <span onClick={
                        () => {
                            this.props.dispatch(routeActions.push('/'));
                        }}>
                        <Glyphicon bsSize='large' glyph='chevron-left'/>
                        Back to Home
                    </span>
                    <br/>
                    <h2>

                    </h2>
                </Col>
                {metrics.map((metric, i) =>{
                    return (
                        <Chart data={metric} key={i}/>
                    );
                })}
            </div>
        );
    }
}

Details.PropTypes = {
    actions: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    metrics: PropTypes.array.isRequired
};

function mapStateToProps ({AppReducer}) {
    return {
        showPopup: AppReducer.showPopup,
        metrics: AppReducer.metrics
    };
}

function mapDispatchToProps (dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch),
        dispatch
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Details);


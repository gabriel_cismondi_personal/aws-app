import * as toDoActions from '../actions/Actions';
//import * as mocks from '../utils/Mock';
import ConfigRegion from './ConfigRegion';

const regions = ['eu-west-1', 'eu-central-1'];

export default function callAPIMiddleware () {
    return (next) => (action) => {
        const {promise} = action;
        if (!promise) {
            return next(action);
        }

        if (promise === 'cw.checkAlarms') {
            let dispatch = action.dispatch;
            let alarms = [];
            for (let i=0; i<regions.length; i++) {
                ConfigRegion(regions[i]);
                let cloudwatch = new AWS.CloudWatch();
                cloudwatch.describeAlarms().
                    on('error', () => {
                        console.log('error making AWS request'); // an error occurred
                    }).
                    on('success', (response) => {
                        console.log(response.data);
                        response.data.MetricAlarms.forEach((metricAlarm) => {
                            alarms.push(metricAlarm);
                        });
                        console.log(alarms);
                        if (i == regions.length -1) dispatch(toDoActions.showPopup(alarms));
                    }).
                    send();
            }
        }

        if (promise == 'ec2.describeInstances') {
            let dispatch = action.dispatch;
            let ec2 = {};
            let ec2Instances = [];
            for (let i=0; i<regions.length; i++) {
                ConfigRegion(regions[i]);
                ec2 = new AWS.EC2();
                ec2.describeInstances().
                    on('error', () => {
                        console.log('error making AWS request'); // an error occurred
                    }).
                    on('success', (response) => {
                        if (response.data.Reservations.length > 0) {
                            response.data.Reservations[0].Instances.forEach((inst) => {
                                inst.Region = regions[i];
                                ec2Instances.push(inst);
                            });
                        }
                        if (i == regions.length -1) dispatch(toDoActions.updateInstances(ec2Instances));
                    }).
                    on('complete', () => {
                        console.log('ended AWS request');
                    }).
                    send();
            }
        }

        if (promise == 'getMetricsList') {
            let instance = action.instance;
            let dispatch = action.dispatch;
            AWS.config.update({
                region: 'eu-west-1',
                credentials: cred
            });
            let par = {
                Dimensions: [
                    {
                        Name: 'InstanceId',
                        Value: 'i-04876762bbc004220'
                    }]
            };
            let cloudwatch = new AWS.CloudWatch();
            cloudwatch.listMetrics(par).
                on('error', (resp) => {
                    console.log('error making AWS request' + resp.data); // an error occurred
                }).
                on('success', (response) => {
                    console.log(response.data);
                    dispatch(toDoActions.getInstanceMetrics(response.data, instance, dispatch));
                }).
                send();
        }

        if (promise == 'ec2.getMetrics') {
            //let instance = action.instance;
            let dispatch = action.dispatch;
            let metricsList = action.metrics.Metrics;
            let date = new Date();
            //let now = new Date(date.getTime());
            let start = new Date('May 3 12:15:00 2016');
            //let yesterday = new Date(date.setDate(date.getDate() - 1));
            let end = new Date('May 3 12:40:00 2016');
            //yesterday.setHours(now.getHours() - 2);
            let cloudwatch = new AWS.CloudWatch();
            //let counter = 0;
            let metricsValues = [];
            //metricsList.forEach((metric) =>{
            let params = {
                EndTime: end,
                StartTime: start,
                Namespace: 'AWS/EC2',
                Period: 300,
                MetricName: 'DiskReadOps',
                Statistics: ['Sum'],
                Dimensions: [
                    {
                        Name: 'InstanceId',
                        Value: 'i-04876762bbc004220'
                    }]
            };
            cloudwatch.getMetricStatistics(params).
                on('error', (response) => {
                    console.log('error making AWS request' + response); // an error occurred
                }).
                on('success', (response) => {
                    console.log(response.data);
                    //counter +=1;
                    metricsValues.push(response.data);
                    //if (counter === (metricsList.length)) dispatch(toDoActions.updateMetrics(metricsValues));
                    dispatch(toDoActions.updateMetrics(metricsValues));
                }).
                send();
            //});
        }
    };
}
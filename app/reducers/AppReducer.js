import * as types from '../constants/ActionTypes';

export const initialState = {
    instances: [],
    filtered: [],
    filter: {
        region: '',
        tag: ''
    },
    showPopup: false,
    metrics: [],
    selectedInstance: {},
    alarms: []
};

export default function AppReducer (state = initialState, action = {}) {
    function filter (instances, filter) {
        let filterdInstances = [];
        instances.forEach((inst) => {
            if ((filter.region == '' && filter.tag == '') ||
                (filter.region == inst.Region && filter.tag == '')
            ) filterdInstances.push(inst);
        });
        if (filterdInstances.length > 0) {
            return filterdInstances;
        } else {
            return instances;
        }
    }
    switch (action.type) {
    case types.UPDATE_INSTANCES:
        console.log(action.retInstance);
        return {
            ...state,
            instances: action.retInstance,
            filtered: action.retInstance

        };
    case types.FILTER_INSTANCES:
        return {
            ...state,
            filtered: filter(action.instances, action.filter)
        };
    case types.DESCRIBE_INSTANCES_FAILURE:
        return {
            ...state
        };
    case types.GET_METRICS_SUCCESS:
        return {
            ...state
        };
    case types.UPDATE_METRICS:
        return {
            ...state,
            metrics: action.metrics
        };
    case types.SELECT_INSTANCE:
        return {
            ...state,
            selectedInstance: action.instance
        };
    case types.SHOW_POPUP:
        let toShow = action.alarms.length > 0;
        return {
            ...state,
            showPopup: toShow,
            alarms: action.alarms
        };
    case types.HIDE_POPUP:
        return {
            ...state,
            showPopup: false
        };
    default:
        return state;
    }
}
